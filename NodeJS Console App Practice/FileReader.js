﻿const EventEmitter = require('events');
const FSObj = require('fs');

class FileReader extends EventEmitter
{
    //gets all the files out of the directory passed
    ReadDirectionFiles(_dir) {
    FSObj.readdir(_dir, (err, files) => {
        if (err) console.log(err);//if there is an error,tell me
        //check if there are files there,just in case we are in an empty directory
        //otherwise,raise the event to call the 'FilesRead' listner,this way this can be repeated easily
        else if (files) this.emit('FilesRead', files);
    });
}
}
//export the class here,we can put more functions in the class if we want and call them off of this
module.exports = FileReader;

