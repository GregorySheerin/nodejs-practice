//ref to the Ref Module,use const to avoid it being changed,it should never need to be changed
var CalcObj = require('./CalcNumbers.js'); //file is in the same dir as app.ts,so no need to find it
var FileReader = require('./FileReader.js');
var FileReaderObj = new FileReader();
var EventEmitter = require('events');
//Register a listener,this will be fired later
//pass the files from the Read Direction function into this
FileReaderObj.on('FilesRead', function (ArgFiles) {
    console.log(ArgFiles);
});
//dummy var for directory,this is just the location of were this file is at the moment
var dir = 'C:\\';
FileReaderObj.ReadDirectionFiles(dir);
//# sourceMappingURL=app.js.map