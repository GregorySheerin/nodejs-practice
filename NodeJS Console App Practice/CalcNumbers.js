﻿//Dont want to have to repeat this text several times,so store it here
var ErrorNotNumbers = "is not a number!";
//Error will be generated depending on what pass in var is not a number
//now regardless of what functions we want to use,the error message should reflect what the user puts in
var Error = "";

function GenerateErrorMSG(_num)
{
    Error = _num + " " + ErrorNotNumbers;
}

function CheckIfNumbers(_num1, _num2) {

    if (isNaN(_num1)) {
        GenerateErrorMSG(_num1);
        return false;
    }
    else if (isNaN(_num2)) {
        GenerateErrorMSG(_num2);
        return false;
    } 
    else {
        return true;
    }

}

function AddNumbers(_num1, _num2)
{
    if (CheckIfNumbers(_num1,_num2)) {
        return _num1 + _num2;
    }
    else {
        return Error;
    }
    
}

function MultiNumbers(_num1, _num2)
{
    if (CheckIfNumbers(_num1, _num2)) {
        return _num1 * _num2;
    }
    else {
        return Error;
    }
}

function SubNumbers(_num1, _num2)
{
    if (CheckIfNumbers(_num1, _num2)) {
        //Take the smaller numbers from the bigger numbers to avoid negative results
        return Math.max(_num1, _num2) - Math.min(_num1, _num2);
    }
    else {
        return ErrorNotNumbers;
    }
}

//export any functions I want to use externaly
module.exports = {
    AddNumbers,
    MultiNumbers,
    SubNumbers
};


