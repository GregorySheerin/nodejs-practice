﻿

//ref to the Ref Module,use const to avoid it being changed,it should never need to be changed
const CalcObj = require('./CalcNumbers.js'); //file is in the same dir as app.ts,so no need to find it
const FileReader = require('./FileReader.js');
const FileReaderObj = new FileReader();
const EventEmitter = require('events');

//Register a listener,this will be fired later
//pass the files from the Read Direction function into this
FileReaderObj.on('FilesRead',(ArgFiles) => {
    console.log(ArgFiles);
})
//dummy var for directory,just using base C directory for example
var dir = 'C:\\';
FileReaderObj.ReadDirectionFiles(dir);








